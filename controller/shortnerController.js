const PostgresSQL_Services = require('../services/postgresServices');
var randomize = require('randomatic');

//function to add url and shortcode
async function addUrl(req, res) {
    try {
        let data = req.body;
        let pass_url = data.url;
        let pass_shortcode = data.shortcode;
        if (!pass_url) {
            let result = {
                statuscode: 400,
                message: `url cannot be empty.`,
            };
            return res.json(result);
        }

        let is_url_Valid = await isUrlValid(pass_url);
        if (!is_url_Valid) {
            let result = {
                statuscode: 400,
                message: `Invalid URL syntax.`,
            };
            return res.json(result);
        }

        if (!pass_shortcode) {
            let check_url_exist = await PostgresSQL_Services.checkUrlExist(pass_url);
            if (check_url_exist) {
                let result = {
                    statuscode: 400,
                    message: `Url already registered.`,
                    shortcode: `${check_url_exist.shortcode}`
                };
                return res.json(result);
            }
            shortcode = randomize('Aa0', 6);
            let insert_url_id = await PostgresSQL_Services.addUrl(pass_url, shortcode);
            let insert_stats = await PostgresSQL_Services.addStats(insert_url_id, 1);
            if (insert_stats) {
                let result = {
                    statuscode: 200,
                    message: `Url registered successfully.`,
                    Shortcode: `${shortcode}`
                };
                return res.json(result);
            } else {
                let result = {
                    statuscode: 400,
                    message: `Error while registering URL.`,
                }
                return res.json(result);
            }
        } else {
            let is_shorcode_valid = await PostgresSQL_Services.getShortcode(pass_url, pass_shortcode);
            if (!is_shorcode_valid) {
                let result = {
                    statuscode: 400,
                    message: `Invalid url or shortcode.`,
                }
                return res.json(result);
            }
            let result = {
                statuscode: 200,
                shortcode: `${is_shorcode_valid.shortcode}`,
            }
            return res.json(result);
        }
    } catch (e) {
        let errMsg = `There was Error ${e}`;
        console.log(errMsg);
    }
}


//function to get stats on shortcode
async function getStats(req, res) {
    let shortcode = req.params.shortcode;
    try {
        let get_stats = await PostgresSQL_Services.getStats(shortcode);
        if (!get_stats) {
            let result = {
                statuscode: 400,
                message: `There are no stats for this shortcode.`,
            }
            return res.json(result);
        }
        let result = {
            statuscode: 200,
            shortcode_accessed_count: `${get_stats.shortcode_accessed_count}`,
            registered_date: `${get_stats.created_at}`,
            last_access_date: `${get_stats.last_access_date}`,
        }
        return res.json(result);
    } catch (e) {
        let errMsg = `There was Error ${e}`;
        console.log(errMsg);
    }
}


//function to check if it is an url
function isUrlValid(url) {
    var res = url.match(/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/g);
    if (res == null)
        return false;
    else
        return true;
}


//function to get url based on shortcode
async function getUrlBasedOnShortcode(req, res) {
    try {
        let shortcode = req.params.shortcode;
        let is_shortcode_exist = await PostgresSQL_Services.getUrlBasedOnShortcode(shortcode);
        let message = '';
        if (!is_shortcode_exist) {
            let result = {
                statuscode: 400,
                message: 'Invalid Shortcode'
            };
            return res.json(result);
        } else {
            let update_stats = await PostgresSQL_Services.updateStats(is_shortcode_exist.id);
            let result = {
                statuscode: 200,
                url: is_shortcode_exist.url
            };
            return res.json(result);
        }
    } catch (e) {
        let errMsg = `There was Error ${e}`;
        console.log(errMsg);
    }
}

module.exports = {
    addUrl,
    getStats,
    isUrlValid,
    getUrlBasedOnShortcode,
};