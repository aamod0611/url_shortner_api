const { describe, it } = require('mocha');
const { stub } = require('sinon');

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);

const { getUrlBasedOnShortcode, addUrl, getStats } = require('../../controller/shortnerController');
const PostgresSQL_Services = require('../../services/postgresServices');

describe('Check URL Exits', () => {

    //get url based on shortcode
    describe('URLs', () => {
        let req = { params: { shortcode: 'vAJlNz' } };
        let res = {
            json: function(obj) {
                return obj;
            }
        };
        describe('/urls ', () => {
            it('it should GET all the url details', async() => {
                const result = await getUrlBasedOnShortcode(req, res);
                console.log("result", result)
                result.statuscode.should.be.eql(200);
            });
        });
    });

    //Register URL if not present in database
    describe('register URL if not exist in the database', () => {
        let req = { body: { url: 'www.gim.com' } };
        let res = {
            json: function(obj) {
                return obj;
            }
        };
        describe('/urls ', () => {
            it('it should return statuscode as 200', async() => {
                const result = await addUrl(req, res);
                console.log("result", result)
                result.statuscode.should.be.eql(200);
            });
        });
    });

    //Check valid url syntax
    describe('Invalid url syntax', () => {
        let req = { body: { url: 'www. coop.com' } };
        let res = {
            json: function(obj) {
                return obj;
            }
        };
        describe('/urls ', () => {
            it('it should return statuscode as 400', async() => {
                const result = await addUrl(req, res);
                console.log("result", result)
                result.statuscode.should.be.eql(400);
            });
        });
    });

    //Check if URL is empty
    describe('URL cannot be empty', () => {
        let req = { body: { url: '' } };
        let res = {
            json: function(obj) {
                return obj;
            }
        };
        describe('/urls ', () => {
            it('it should return statuscode as 400', async() => {
                const result = await addUrl(req, res);
                console.log("result", result)
                result.statuscode.should.be.eql(400);
            });
        });
    });

    // Invalid url or shortcode if either is wrong
    describe('Invlaid Url or shortcode', () => {
        let req = { body: { url: 'www.goop.com', shortcode: 'ABCD' } };
        let res = {
            json: function(obj) {
                return obj;
            }
        };
        describe('/urls ', () => {
            it('it should return statuscode as 400', async() => {
                const result = await addUrl(req, res);
                console.log("result", result)
                result.statuscode.should.be.eql(400);
            });
        });
    });

    //Invalid url or shortcode if either is wrong
    describe('get stats from shortcode', () => {
        let req = { params: { shortcode: 'MtlH1Q' } };
        let res = {
            json: function(obj) {
                return obj;
            }
        };
        describe('/urls ', () => {
            it('it should return statuscode as 200', async() => {
                const result = await getStats(req, res);
                console.log("result", result)
                result.statuscode.should.be.eql(200);
            });
        });
    });

})