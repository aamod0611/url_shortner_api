// getting env variables.
require("dotenv").config();
const Sequelize = require('sequelize');

const sequelize = new Sequelize(process.env.PG_DB, process.env.PG_USER, process.env.PG_PASSWORD, {
    host: process.env.PG_HOST,
    dialect: 'postgres'
});


//function to add url
const addUrl = async(url, shortcode) => {
    try {
        await sequelize.authenticate();
        let row = await sequelize.query("INSERT INTO url_details (url,shortcode) VALUES ( :url, :shortcode)", {
            replacements: {
                url: url,
                shortcode: shortcode,
            },
            type: sequelize.QueryTypes.INSERT
        });
        let last_id = await sequelize.query("SELECT currval('url_details_id_seq')");
        return last_id[0][0].currval;
    } catch (error) {
        console.log(error);
        throw error;
    }
}


//function to add stats for the first time
const addStats = async(url_id, shortcode_accessed_count) => {
    try {
        await sequelize.authenticate();
        let row = await sequelize.query("INSERT INTO stats (url_id, shortcode_accessed_count) VALUES ( :url_id, :shortcode_accessed_count)", {
            replacements: {
                url_id: url_id,
                shortcode_accessed_count: shortcode_accessed_count,
            },
            type: sequelize.QueryTypes.SELECT
        });
        return true
    } catch (error) {
        console.log(error);
        throw error;
    }
}


//function to get stats
const getStats = async(shortcode) => {
    try {
        await sequelize.authenticate();
        let row = await sequelize.query("SELECT * FROM url_details where shortcode = :shortcode", {
            replacements: {
                shortcode: shortcode,
            },
            type: sequelize.QueryTypes.SELECT
        });
        if (row.length) {
            let stats = await sequelize.query("SELECT * FROM stats where url_id = :url_id", {
                replacements: {
                    url_id: row[0].id,
                },
                type: sequelize.QueryTypes.SELECT
            });
            if (stats.length) {
                return stats[0];
            }
            return false;
        }
        return false;

    } catch (error) {
        console.log(error);
        throw error;
    }
}


//function to update stats
const updateStats = async(url_id) => {
    try {
        var updated_at = new Date().toISOString()
        await sequelize.authenticate();
        let insert_row = await sequelize.query("SELECT * FROM stats where url_id = :url_id", {
            replacements: {
                url_id: url_id,
            },
            type: sequelize.QueryTypes.SELECT
        });
        let shortcode_count = parseInt(insert_row[0].shortcode_accessed_count);
        shortcode_count = shortcode_count + 1;

        let update_row = await sequelize.query("update stats set shortcode_accessed_count = :shortcode_count, last_access_date = :updated_at where url_id = :url_id", {
            replacements: {
                shortcode_count: shortcode_count,
                updated_at: updated_at,
                url_id: url_id,
            },
            type: sequelize.QueryTypes.UPDATE
        });
        console.log(update_row);
    } catch (error) {
        console.log(error);
        throw error;
    }
}


//function to return shortcode based on shortcode and url
const getShortcode = async(url, shortcode) => {
    try {
        await sequelize.authenticate();
        let row = await sequelize.query("SELECT * FROM url_details where url = :url and shortcode = :shortcode", {
            replacements: {
                url: url,
                shortcode: shortcode,
            },
            type: sequelize.QueryTypes.SELECT
        });
        if (row.length) {
            return row[0];
        }
        return false;

    } catch (error) {
        console.log(error);
        throw error;
    }
}


//function to check if url already registered
const checkUrlExist = async(url) => {
    try {
        await sequelize.authenticate();
        let row = await sequelize.query("SELECT * FROM url_details where url = :url", {
            replacements: {
                url: url,
            },
            type: sequelize.QueryTypes.SELECT
        });
        if (row.length) {
            return row[0];
        }
        return false;

    } catch (error) {
        console.log(error);
        throw error;
    }
}


//function to return url based on shortcode
const getUrlBasedOnShortcode = async(shortcode) => {
    try {
        await sequelize.authenticate();
        let row = await sequelize.query("SELECT * FROM url_details where shortcode = :shortcode", {
            replacements: {
                shortcode: shortcode,
            },
            type: sequelize.QueryTypes.SELECT
        });
        if (row.length) {
            return row[0];
        }
        return false;

    } catch (error) {
        console.log(error);
        throw error;
    }
}

module.exports = {
    addUrl,
    addStats,
    getStats,
    updateStats,
    getShortcode,
    checkUrlExist,
    getUrlBasedOnShortcode
};