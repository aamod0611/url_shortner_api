var express = require('express');
var router = express();
var ShortnerController = require('../controller/shortnerController');

//API to store urls
router.post('/urls', ShortnerController.addUrl);

//API to get url based on shortcode
router.get('/urls/:shortcode', ShortnerController.getUrlBasedOnShortcode);

//API to get stats based on shortcode
router.get('/urls/:shortcode/stats', ShortnerController.getStats);


module.exports = router;