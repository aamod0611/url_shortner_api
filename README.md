# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Description ###

* API for url-shortener app.

### Pre-requisites ###

1. [Node Js Setup](https://nodejs.org/en/download/package-manager/)
2. [Postgres Setup](https://postgresapp.com/)

### Setup node modules ###

* Run npm install inside root directory.

### Run and Test API Locally ###

* Execute npm start from root directory

### Run Test Cases ###

* Execute npm test from root directory.
